FROM buildroot/base:latest

ENV THREADS=1
ENV DEFCONFIG=""

RUN git clone git://git.buildroot.net/buildroot
RUN mkdir -p /home/br-user/buildroot/output/images

WORKDIR /home/br-user/buildroot

ENTRYPOINT /usr/bin/make $DEFCONFIG; echo "========== Start making \"$DEFCONFIG\" with $THREADS threads"; make -j $THREADS