# Syn Linux Builder

Syn_linux_builder is a utility for creating a custom Linux image based on defconfig using temporary Docker container.

## Installation
If you don't have Docker on your machine, install it using the command below or just refer to the [official Docker documentation](https://docs.docker.com/engine/install/): 
```bash
# for Ubuntu
sudo apt update
sudo apt install docker.io
```
Use the code below in your working directory to get the Dockerfile and build a Docker image:
```bash
git clone git@gitlab.com:pavel1549/synopsys_test_task.git
docker build -t syn_linux_builder . 
```
Or if you don't want to build the Docker image by your own, you can download the run ready image from Docker hub:
```bash
docker pull pavel1549/syn_linux_builder
```

## Usage
The container stopping automatically after Linux image created.

```bash
docker run -d --rm \
  -e DEFCONFIG=qemu_aarch64_virt_defconfig \
  -e THREADS=9 \
  -v /source/dir/:/home/br-user/buildroot/output/images \
  syn_linux_builder

# -d  - to run container as a daemon (in example above)
# -it - use these options instead of -d to run container in interactive mode
# -e DEFCONFIG=<defconfig_name> - required option (env variable): specifies a defconfig name to build a Linux image
# -e THREADS=<number> - specifies the number of threads used for building an image. 1 thread is used by default. You can run lscpu to display the number of available cores and threads.
# -v /source/dir/:/home/br-user/buildroot/output/images - replace /source/dir with your directory where the result Linux image and related output files will be placed.
# --rm - the option means that after Linux created the container will be automatically removed.

After all you can use the created image in QEMU Simulator or on a real machine.
Good luck!
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
